package com.example.demo;

public class Rooms {

	private String name;
	private String description;
	private long id;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getid() {
		return id;
	}
	public void setid(long id) {
		this.id = id;
	}
	
	
	public Rooms() {
		
	}
	public Rooms(String name, String description, long id) {
		this.name = name;
		this.description = description;
		this.id = id;
	}
	@Override
	public String toString() {
		String info = String.format("Rooms Info: name = %s, description = %s, id = %d",
				                     name, description, id);
		return info;
	}
	
	
}
