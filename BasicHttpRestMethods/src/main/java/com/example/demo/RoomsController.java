package com.example.demo;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class RoomsController {

	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public String post(@RequestBody Rooms cust) {
		System.out.println("/POST request, cust: " + cust.toString());
		return "/Post Successful!";
	}

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public Rooms get(@RequestParam("name") String name, @RequestParam("description") String description, @RequestParam("id") long  id  ) {
		String info = String.format("/GET info: name=%s, description=%s, id=%d", name,description, id);		
		System.out.println(info);
		return new Rooms(name,description, id);
	}

	@RequestMapping(value= "/put/{id}", method = RequestMethod.PUT)
	public void put(@PathVariable(value = "id") long id, @RequestBody Rooms cust) {
		String info = String.format("id = %d, custinfo = %s", id, cust.toString());
		System.out.println("/PUT info " + info);
	}
	
	@RequestMapping(value= "/delete/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable(value = "id") long id) {
		String info = String.format("/Delete info: id = %d", id);
		System.out.println(info);
  }
}