package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.data.RoomRepository;
import com.example.demo.model.Room;


@RestController
public class RoomController {

	@Autowired
	private RoomRepository roomData;

	@RequestMapping(value = "/listPageable", method = RequestMethod.GET)
	Page<Room> roomsPageable(Pageable pageable) {
		return roomData.findAll(pageable);

	}
}
